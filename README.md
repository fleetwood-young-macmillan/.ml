# sup nerds

Clone this into your `macmillan/.ml` folder.

Your local should look like this when you're done:

```
macmillan/

├── .ml
│   ├── environments
│   │   ├── dev-courseware/stuff
│   │   ├── dev-cw-coursemgmt1/stuff
│   │   ├── blah, blah, blah
│   └── config.yml
├── psv-services
├── ml-sdk
└── other-macmillan-stuff
```